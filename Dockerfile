FROM centos:7
MAINTAINER "The Quattro Project Team"

ENV DOCKER_CLIENT_VERSION 1.12.6-11.el7.centos

RUN yum -y install docker-client-${DOCKER_CLIENT_VERSION}.x86_64 && yum clean all
